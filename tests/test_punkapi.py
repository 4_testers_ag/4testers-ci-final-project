import requests


# Zadanie 1
def test_default_beers():
    response = requests.get("https://api.punkapi.com/v2/beers")
    body = response.json()
    assert body[0]["id"] == 1
    assert body[-1]["id"] == 25
    assert len(body) == 25


# Zadanie 2
def test_get_beer_with_id_123():
    response = requests.get("https://api.punkapi.com/v2/beers/123")
    body = response.json()
    assert body[0]["id"] == 123
    assert body[0]["name"] == "Candy Kaiser"


# Zadanie 3
def test_get_20_beers_from_5th_page():
    params = {
        "page": 5,
        "per_page": 20
    }
    response = requests.get("https://api.punkapi.com/v2/beers", params=params)
    body = response.json()
    assert len(body) == 20
    assert body[0]["id"] == 81
    assert body[19]["id"] == 100


# Zadanie 4
def test_ids_11_20():
    params = {
        "ids": "11|12|13|14|15|16|17|18|19|20"}
    response = requests.get("https://api.punkapi.com/v2/beers", params=params)
    body = response.json()
    assert len(body) == 10

    beer_id = 11
    for beer in body:
        assert beer["id"] == beer_id
        beer_id = beer_id + 1


# Zadanie 5
def test_abv_between_5_and_7():
    params = {
        "abv_gt": 5,
        "abv_lt": 7
    }
    response = requests.get("https://api.punkapi.com/v2/beers", params=params)
    body = response.json()

    for beer in body:
        abv = beer["abv"]
        assert abv > 5 and abv < 7


# Zadanie 6
def test_beers_produced_in_2010():
    params = {
        "brewed_before": "01-2011",
        "brewed_after": "12-2009"
    }
    response = requests.get("https://api.punkapi.com/v2/beers", params=params)
    body = response.json()

    for beer in body:
        date = beer["first_brewed"]
        assert date.endswith("2010")





